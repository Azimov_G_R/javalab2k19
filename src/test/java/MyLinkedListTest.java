import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.*;

public class MyLinkedListTest {

    @Test
    public void getElementByIndex() {
        MyLinkedList<String> mass = new MyLinkedList<>();
        mass.addFirst("L");
        mass.addFirst("a");
        mass.addFirst("B");

        String actual = mass.getElementByIndex(1);
        String expected = "a";
        assertEquals(expected, actual);
    }

    @Test
    public void addFirst() {
        MyLinkedList<String> mass = new MyLinkedList<>();
        mass.addFirst("L");
        mass.addFirst("a");
        mass.addFirst("B");

        String actual = mass.getElementByIndex(1);
        String expected = "a";
        assertEquals(expected, actual);
    }

    @Test
    public void addLast() {
        MyLinkedList<String> mass = new MyLinkedList<>();
        mass.addLast("L");
        mass.addLast("a");
        mass.addLast("B");
        String actual = mass.getElementByIndex(mass.size()-1);
        String expected = "B";
        assertEquals(expected, actual);
    }

    @Test
    public void addByIndex() {
        MyLinkedList<String> mass = new MyLinkedList<>();
        mass.addFirst("B");
        mass.addLast("a");
        mass.addByIndex(1, "ADD");
        LinkedList<String> something = new LinkedList<>();
        something.addFirst("B");
        something.addLast("a");
        something.add(1, "ADD");
        String actual = mass.getElementByIndex(1);
        String expected = something.get(1);
        assertEquals(expected, actual);
    }

    @Test
    public void removeFirst() {
        MyLinkedList<String> mass = new MyLinkedList<>();
        mass.addFirst("B");
        mass.addLast("a");
        mass.addByIndex(1, "ADD");
        mass.removeFirst();
        LinkedList<String> something = new LinkedList<>();
        something.addFirst("B");
        something.addLast("a");
        something.add(1, "ADD");
        something.removeFirst();
        String actual = mass.getElementByIndex(0);
        String expected = something.get(0);
        assertEquals(expected, actual);
    }

    @Test
    public void removeLast() {
        MyLinkedList<String> mass = new MyLinkedList<>();
        mass.addFirst("B");
        mass.addLast("a");
        mass.addByIndex(1, "ADD");
        mass.removeLast();
        LinkedList<String> something = new LinkedList<>();
        something.addFirst("B");
        something.addLast("a");
        something.add(1, "ADD");
        something.removeLast();
        String actual = mass.getElementByIndex(mass.size() - 1);
        String expected = something.get(something.size() - 1);
        assertEquals(expected, actual);
    }

    @Test
    public void removeByIndex() {
        MyLinkedList<String> mass = new MyLinkedList<>();
        mass.addFirst("B");
        mass.addLast("a");
        mass.addByIndex(1, "ADD");
        mass.removeByIndex(1);
        LinkedList<String> something = new LinkedList<>();
        something.addFirst("B");
        something.addLast("a");
        something.add(1, "ADD");
        something.remove(1);
        String actual = mass.getElementByIndex(1);
        int sizeAct = mass.size();
        String expected = something.get(1);
        int sizeExpec = something.size();
        assertEquals(expected, actual);
        assertEquals(sizeExpec, sizeAct);
    }

    @Test
    public void clear() {
        MyLinkedList<String> mass = new MyLinkedList<>();
        mass.addFirst("B");
        mass.addLast("a");
        mass.clear();
        LinkedList<String> something = new LinkedList<>();
        something.addFirst("B");
        something.addLast("a");
        something.clear();
        int sizeAct = mass.size();
        int sizeExpec = something.size();
        assertEquals(sizeAct, sizeExpec);

    }

    @Test
    public void size() {
        MyLinkedList<String> mass = new MyLinkedList<>();
        mass.addFirst("B");
        mass.addLast("a");
        int sizeAct = mass.size();
        int sizeExpec = 2;
        assertEquals(sizeAct, sizeExpec);
    }


    @Test
    public void setElementByIndex() {
        MyLinkedList<String> mass = new MyLinkedList<>();
        mass.addFirst("B");
        mass.addLast("a");
        mass.setElementByIndex(1, "ADD");
        LinkedList<String> something = new LinkedList<>();
        something.addFirst("B");
        something.addLast("a");
        something.set(1, "ADD");
        String actual = mass.getElementByIndex(1);
        int sizeAct = mass.size();
        String expected = something.get(1);
        int sizeExpec = something.size();
        assertEquals(expected, actual);
        assertEquals(sizeExpec, sizeAct);
    }

    @Test
    public void toList() {
        MyLinkedList<String> mass = new MyLinkedList<>();
        mass.addFirst("B");
        mass.addLast("a");
        LinkedList<String> something = new LinkedList<>();
        something.addFirst("B");
        something.addLast("a");
        String actual = mass.getElementByIndex(1);
        String expected = something.get(1);
        assertEquals(actual, expected);

    }

}