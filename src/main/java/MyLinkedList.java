public class MyLinkedList<E> implements MyList<E> {


    private Node<E> firstNode;
    private Node<E> lastNode;
    private int size = 0;

    public MyLinkedList() {
        lastNode = new Node<E>(null, firstNode, null);
        firstNode = new Node<E>(null, null, lastNode);
    }


    @Override
    public void addFirst(E e) {

        if (firstNode.getOurElement() == null && lastNode.getOurElement()==null) {
            Node<E> node = new Node<E>(e, null, null);
            firstNode = node;
            lastNode = node;
            size++;
        }else
        if (firstNode.getOurElement() != null && lastNode.getOurElement()==null) {
            firstNode.setOurElement(e);
            lastNode.setPrevElement(firstNode);
            size++;
        } else {
            final Node<E> next = firstNode;
            final Node<E> node = new Node<>(e, null, next);
            firstNode = node;
            next.setPrevElement(firstNode);
            size++;
        }

    }

    @Override
    public void addLast(E e) {
        if (firstNode.getOurElement() == null && lastNode.getOurElement()==null) {
            Node<E> node = new Node<E>(e, null, null);
            firstNode = node;
            lastNode = node;
            size++;
        }else
        if (firstNode.getOurElement() != null && lastNode.getOurElement()==null) {
            lastNode.setOurElement(e);
            firstNode.setNextElement(lastNode);
            size++;
        } else {
            final Node<E> prev = lastNode;
            final Node<E> node = new Node<>(e, prev, null);
            lastNode = node;
            prev.setNextElement(lastNode);
            size++;
        }
    }


    @Override
    public void addByIndex(int index, E e) {
        if (firstNode.getOurElement() != null && size >= index) {

            Node<E> temp = firstNode;
            for (int i = 0; i < index; i++) {
                temp = getNextElem(temp);
            }
            Node<E> prev = temp.prevElement;
            Node<E> target = temp;
            temp = new Node<>(e, prev, target);
            prev.setNextElement(temp);
            target.setPrevElement(temp);
            size++;
        }
    }

    @Override
    public void removeFirst() {
        Node<E> start = firstNode;
        Node<E> next = firstNode.getNextElement();
        start.setNextElement(null);
        next.setPrevElement(null);
        firstNode = next;
        size--;

    }

    @Override
    public void removeLast() {
        Node<E> end = lastNode;
        Node<E> prev = lastNode.getPrevElement();
        end.setPrevElement(null);
        prev.setNextElement(null);
        lastNode = prev;
        size--;
    }

    @Override
    public void removeByIndex(int index) {
        Node<E> temp = firstNode;
        for (int i = 0; i < index; i++) {
            temp = getNextElem(temp);
        }
        Node<E> next = temp.nextElement;
        Node<E> prev = temp.prevElement;
        prev.setNextElement(next);
        next.setPrevElement(prev);
        size--;

    }

    @Override
    public void clear() {
        firstNode.setOurElement(null);
        lastNode.setOurElement(null);
        firstNode.setNextElement(lastNode);
        lastNode.setPrevElement(firstNode);
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public E getElementByIndex(int index) {
        Node<E> temp = firstNode;
        for (int i = 0; i < index; i++) {
            temp = getNextElem(temp);
        }
        return temp.getOurElement();
    }


    private Node<E> getNextElem(Node<E> current) {
        return current.getNextElement();
    }

    @Override
    public void setElementByIndex(int index, E e) {
        Node<E> temp = firstNode;
        for (int i = 0; i < index; i++) {
            temp = getNextElem(temp);
        }
        temp.setOurElement(e);

    }

    @Override
    public void toList() {

        if (size > 1) {
            Node<E> temp = firstNode;
            for (int i = 0; i < size; i++) {
                System.out.println(temp.getOurElement());
                temp = getNextElem(temp);
            }
        } else if (size == 1) {
            Node<E> temp = firstNode;
            System.out.println(temp.getOurElement());
        }
    }


    private class Node<E> {
        private E ourElement;
        private Node<E> nextElement;
        private Node<E> prevElement;

        private Node(E ourElement, Node<E> prevElement, Node<E> nextElement) {
            this.ourElement = ourElement;
            this.nextElement = nextElement;
            this.prevElement = prevElement;
        }

        public E getOurElement() {
            return ourElement;
        }

        public void setOurElement(E ourElement) {
            this.ourElement = ourElement;
        }

        public Node<E> getNextElement() {
            return nextElement;
        }

        public void setNextElement(Node<E> nextElement) {
            this.nextElement = nextElement;
        }

        public Node<E> getPrevElement() {
            return prevElement;
        }

        public void setPrevElement(Node<E> prevElement) {
            this.prevElement = prevElement;
        }
    }

}