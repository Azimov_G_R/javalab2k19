public interface MyList<E> {
    void addFirst(E e);
    void addLast(E e);
    void addByIndex(int index, E e);
    void removeFirst();
    void removeLast();
    void removeByIndex(int index);
    void clear();
    int size();
    E getElementByIndex(int index);
    void setElementByIndex(int index, E e);
    void toList();
}
